from django.shortcuts import render
# from .forms import SubscriberForm
from my_contact.models import *

def my_contact(request):
	# gallery_images = GalleryImage.objects.filter(is_active = True, is_main=True)
	return render(request, 'my_contact/my_contact.html', locals())
